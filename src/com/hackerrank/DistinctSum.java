package com.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DistinctSum {
	public static void main(String[] args) {
		HashMap<Integer, Integer> hmap = new HashMap<Integer, Integer>();
		int[] arr = { 5, 7, 9, 13, 11, 6, 6, 3, 3 };
		int num = 12;
		List<Integer> list = new ArrayList<>(arr.length);
		for (int i : arr) {
			list.add(i);
		}
		System.out.println(list);
		Collections.sort(list);
		System.out.println(list);
		for(int i=0;i<arr.length;i++) {
			for (int j = i + 1; j < arr.length - 1; j++) {
				int sum = 0;
				sum = list.get(i) + list.get(j);
				if (sum == num) {
					hmap.put(list.get(i), list.get(j));
				}
			}
		}
		System.out.println(hmap);
		System.out.println(hmap.size());
	}
}
