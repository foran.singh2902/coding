package com.collectionsdemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class ArrayListDemo {

	public static void main(String[] args) {
		String fruits[] = { "banana", "apple", "orange", "mango", "strawberry" };
		List<String> list = new ArrayList<String>(fruits.length);
		for (String string : fruits) {
			list.add(string);
		}
		System.out.println("list " + list);
		Collections.sort(list);
		System.out.println("after sort " + list);
		// by default item will add at end
		list.add("pineapple");
		System.out.println("list after default add " + list);
		// if we specify an index and at that place a element exists then it will be
		// moved to right
		list.add(4, "papaya");
		System.out.println("after adding to index 4 " + list);
		// editing element at a particular index
		list.set(0, "apples");
		System.out.println("after editing element at a particular index " + list);
		System.out.println("contains papayas ? " + list.contains("papayas"));
		System.out.println("will return -1 if index of particular element not found " + list.indexOf("papayas"));
		ListIterator<String> iterator = list.listIterator();
		// using ListIterator to iterate throught the list
		while (iterator.hasNext()) {
			String string = (String) iterator.next();
			System.out.println(string);
		}
		System.out.println("Size of the arrayList " + list.size());
	}

}
