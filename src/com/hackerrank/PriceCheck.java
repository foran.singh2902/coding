package com.hackerrank;

import java.util.HashMap;
import java.util.Map;

public class PriceCheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double productPrices[] = { 2.89, 3.29, 5.79 };
		String products[] = { "eggs", "milk", "cheese" };
		String productsSold[] = { "eggs", "eggs", "cheese", "milk" };
		double soldPrice[] = { 2.89, 2.99, 5.97, 3.29 };
		Map<String, Double> hmap = new HashMap<String, Double>();
		for (int i = 0; i < products.length; i++) {
			hmap.put(products[i], productPrices[i]);
		}
		int count = 0;
		for (int i = 0; i < productsSold.length; i++) {
			if (hmap.get(productsSold[i]) == soldPrice[i]) {
				++count;
			}
		}
		System.out.println("wrong items sold are :" + count);
	}

}
