package com.collectionsdemo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num[] = { 6, 9, 3, 8, 3, 6, 8, 1, 7, 5 };
		List<Integer> list = new ArrayList<Integer>(num.length);
		for (int i = 0; i < num.length; i++) {
			list.add(num[i]);
		}
		System.out.println("List " + list);
		Set<Integer> set = new HashSet<Integer>();
		// set.add(4);
		set.addAll(list);
		System.out.println("Set :" + set);
		// set.
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}

	}

}
