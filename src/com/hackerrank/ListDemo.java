package com.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num[] = { 2, 3, 4, 7, 1, 9, 5, 4 };
		List<Integer> list = new ArrayList<Integer>(num.length);
		for (int i : num) {
			list.add(i);
		}
		System.out.println("before sorting " + list);
		Collections.sort(list);
		System.out.println("after sorting " + list);
		Set<Integer> set = new HashSet<Integer>(list);
		System.out.println("using set " + set);
	}

}
