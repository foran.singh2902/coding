package com.learnthreads;

public class MainTest extends Thread {

	@Override
	public void run() {
		System.out.println("calling thread of Thread Class");
	}

	public static void main(String[] args) {
		MainTest m = new MainTest();
		m.start();
		Thread t = new Thread(new UsingThread());
		// start method of Thread Class
		t.start();
	}

}
